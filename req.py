#!/usr/bin/env python
# -*- coding: utf-8 -*-
# これはRaspberry piの上で動作させる
# sudo python req.py
import requests
import RPi.GPIO as GPIO
import time

LEDPIN = 24

GPIO.setwarnings(False)	# 警告を出さない

GPIO.setmode( GPIO.BCM )  # BCMモードで
GPIO.setup( LEDPIN, GPIO.OUT )   # LEDPIN＝24を出力モードで

while 1:
    # GETでクラウド上にあるLEDの状態を獲得
    r = requests.get('https://HerokuのURL/ledon')
    # それがOnならLEDを点灯、Offなら消灯。
    if r.text == "On":
        GPIO.output(LEDPIN, True)
    else:
        GPIO.output(LEDPIN, False)
    time.sleep(1.0)
