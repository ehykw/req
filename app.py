#!/usr/bin/env python
#-*- coding: utf-8 -*-
import os
from flask import Flask
#!/usr/bin/env python
#-*- coding: utf-8 -*-
import os
from flask import Flask
from flask import request
from flask import render_template
from flask import abort, redirect, url_for

led_status = "Off"

app = Flask(__name__)

# buttonファイルを表示する
@app.route('/')
def index():
    return render_template('button.html')

# ボタンの処理
@app.route('/ledon', methods=['GET', 'POST'])
def ledon():
    global led_status
    if request.method == 'POST':
        # POSTメソッドならled_statusに現在のLED状態を保持
        led_status = request.form['on']
        return redirect(url_for('index'))
    elif request.method == 'GET':
        # GETメソッドならled_statusの値を返す
        return led_status
    else:
        return redirect(url_for('index'))

if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
